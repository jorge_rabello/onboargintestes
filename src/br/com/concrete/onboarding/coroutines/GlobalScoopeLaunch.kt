import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

suspend fun printDelayedD2(message: String) {
    delay(5000)
    println(message)
}

fun exampleLaunchGlobal() = runBlocking {
    println("one from thread ${Thread.currentThread().name}")

    GlobalScope.launch {
        printDelayedD2("two from thread ${Thread.currentThread().name}")
    }
    println("three from thread ${Thread.currentThread().name}")
    delay(5000)
}


fun main(args: Array<String>) {
    exampleLaunchGlobal()
}


