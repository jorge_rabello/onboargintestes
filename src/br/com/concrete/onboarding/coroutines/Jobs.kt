package br.com.concrete.onboarding.coroutines

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

suspend fun printDelayedD2(message: String) {
    delay(5000)
    println(message)
}

fun exampleLaunchGlobalWaiting() = runBlocking {
    println("one from thread ${Thread.currentThread().name}")

    val job = GlobalScope.launch {
        printDelayedD2("two from thread ${Thread.currentThread().name}")
    }

    println("three from thread ${Thread.currentThread().name}")

    job.join()
}

fun main(args: Array<String>) {
    exampleLaunchGlobalWaiting()
}


