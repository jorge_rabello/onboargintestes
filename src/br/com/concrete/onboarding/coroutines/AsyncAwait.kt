import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

suspend fun calculateHardThings(startNum: Int): Int {
    delay(1000)
    return startNum * 10
}

fun exampleWithContext() = runBlocking {

    val startTime = System.currentTimeMillis()

    val result1 = withContext(Dispatchers.Default) { calculateHardThings(10) }

    val result2 = withContext(Dispatchers.Default) { calculateHardThings(20) }

    val result3 = withContext(Dispatchers.Default) { calculateHardThings(30) }

    val sum = result1 + result2 + result3

    println("async/await result = $sum")

    val endTime = System.currentTimeMillis()

    println("Time taken: ${endTime - startTime}")

}

fun main(args: Array<String>) {
    exampleWithContext()
}
