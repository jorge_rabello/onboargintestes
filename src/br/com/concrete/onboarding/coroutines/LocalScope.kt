package br.com.concrete.onboarding.coroutines

import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

suspend fun printDelayedD3(message: String) {
    delay(5000)
    println(message)
}

fun exampleLaunchCoroutineScope() = runBlocking {
    println("one from thread ${Thread.currentThread().name}")
    this.launch {
        printDelayedD3("two from thread ${Thread.currentThread().name}")
    }

    println("three from thread ${Thread.currentThread().name}")
}

fun main(args: Array<String>) {
    exampleLaunchCoroutineScope()
}

