import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

suspend fun printDelayed5(message: String) {
    delay(5000)
    println(message)
}

fun exampleLaunchCoroutineScope() = runBlocking {
    val customDispatcher = Executors
        .newFixedThreadPool(2)
        .asCoroutineDispatcher()

    println("one from thread ${Thread.currentThread().name}")

    launch(customDispatcher) { printDelayed5("two from thread ${Thread.currentThread().name}") }

    println("three from thread ${Thread.currentThread().name}")

    (customDispatcher.executor as ExecutorService).shutdown()
}

fun main(args: Array<String>) {
    exampleLaunchCoroutineScope()
}
