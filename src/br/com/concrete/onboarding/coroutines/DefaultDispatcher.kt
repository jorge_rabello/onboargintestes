package br.com.concrete.onboarding.coroutines

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

suspend fun printDelayedD(message: String) {
    delay(5000)
    println(message)
}

fun exampleBlockingDispatcher() {
    runBlocking(Dispatchers.Default) {
        println("one from thread ${Thread.currentThread().name}")
        printDelayedD("two from thread ${Thread.currentThread().name}")
    }
    // fora do bloco runBlocking
    println("two from thread ${Thread.currentThread().name}")
}

fun main(args: Array<String>) {
    exampleBlockingDispatcher()
}


