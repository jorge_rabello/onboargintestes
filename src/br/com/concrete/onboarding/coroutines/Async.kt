package br.com.concrete.onboarding.coroutines

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

suspend fun printDelayed(message: String) {
    delay(5000)
    println(message)
}

fun exemploBloqueante() = runBlocking {
    println("one")
    printDelayed("two")
    println("three")
}


fun main(args: Array<String>) {
    exemploBloqueante()
}
