package br.com.concrete.onboarding.testes;

import java.util.Objects;

public class CarroJava {

    private final String cor;
    private final String marca;
    private final int ano;

    public CarroJava(String cor, String marca, int ano) {
        this.cor = cor;
        this.marca = marca;
        this.ano = ano;
    }

    public String getCor() {
        return cor;
    }

    public String getMarca() {
        return marca;
    }

    public int getAno() {
        return ano;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CarroJava carroJava = (CarroJava) o;
        return Objects.equals(cor, carroJava.cor) &&
                Objects.equals(marca, carroJava.marca);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cor, marca);
    }

    @Override
    public String toString() {
        return "CarroJava{" +
                "cor='" + cor + '\'' +
                ", marca='" + marca + '\'' +
                '}';
    }
}
