package br.com.concrete.onboarding.open

import br.com.concrete.onboarding.functions.User

open class UserResult

data class Success(val users: List<User>) : UserResult()
data class Failure(val message: String) : UserResult()
