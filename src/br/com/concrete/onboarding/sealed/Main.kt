package br.com.concrete.onboarding.sealed

import br.com.concrete.onboarding.functions.User

data class MyClass(val users: List<User>) : UserResult()

fun main(args: Array<String>) {
    val userResult = UserResult()
    userResult.toString()
}
