package br.com.concrete.onboarding.sealed

import br.com.concrete.onboarding.functions.User
import br.com.concrete.onboarding.functions.userFromJSONFile

// dessa forma funciona

open class UserResult
data class Success(val users: List<User>) : UserResult()
data class Failure(val message: String) : UserResult()

fun retrieveUsers(): UserResult = Success(userFromJSONFile("user.json"))
