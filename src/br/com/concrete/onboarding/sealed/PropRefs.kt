package br.com.concrete.onboarding.sealed


var intProperty = 2

fun main(args: Array<String>) {

    println(intProperty)

    ::intProperty.set(10)

    println(intProperty)
}
