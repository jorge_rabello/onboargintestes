package br.com.concrete.onboarding.functions

fun sendPayment(money: Money, message: String = "") = println("Sending ${money.amount} $message")
fun sum(x: Int, y: Int) = x + y

fun convertToDollars(money: Money): Money = when (money.currency) {
    "USD", "$" -> money
    "EUR" -> Money(money.amount.times(1.10), "$")
    "BRL" -> Money(money.amount.times(4.67), "$")
    else -> throw IllegalArgumentException("not the currency you're interested in")
}


fun main(args: Array<String>) {

    val usMoney = Money(amount = 10.0, currency = "$")
    val usdMoney = Money(amount = 10.0, currency = "USD")
    val euMoney = Money(amount = 10.0, currency = "EUR")
    val brMoney = Money(amount = 10.0, currency = "BRL")

    println(convertToDollars(usMoney))
    println(convertToDollars(usdMoney))
    println(convertToDollars(euMoney))
    println(convertToDollars(brMoney))
}
