package br.com.concrete.onboarding.functions

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.FileReader

data class User(
    val id: Long,
    val name: String,
    val email: String,
    val role: String,
    val age: Int
)

fun userFromJSONFile(fileName: String): List<User> {
    val gson = Gson()
    return gson.fromJson(FileReader(fileName), object : TypeToken<List<User>>() {}.type)
}

fun retrieveUsers() = userFromJSONFile("users.json")

fun main(args: Array<String>) {

    val result = retrieveUsers()

    result.forEach { user ->
        println("Name: ${user.name} - Age: ${user.age}")
    }

}
