package br.com.concrete.onboarding.functions

data class Money(val amount: Double, val currency: String)
