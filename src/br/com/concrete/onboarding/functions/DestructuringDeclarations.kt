package br.com.concrete.onboarding.functions

fun main(args: Array<String>) {
    val users = userFromJSONFile("users.json")

    val (id, _, email) = users
        .filter { user -> user.email.endsWith(".com") }
        .sortedBy { user -> user.id }
        .first()

    println("ID: $id")
    println("EMAIL: $email")
}
