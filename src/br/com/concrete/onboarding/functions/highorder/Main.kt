import br.com.concrete.onboarding.functions.userFromJSONFile

fun main(args: Array<String>) {
    val users = userFromJSONFile("users.json")

    val dotComUsers = users.filter { user ->
        user.email.endsWith(".com")
    }.sortedBy { user ->
        user.id
    }.map { user ->
        user.email to user.name
    }

    println("Users:\n$users")
    println("\nDot Com Users:\n$dotComUsers")
}
