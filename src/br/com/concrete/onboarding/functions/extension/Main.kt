package br.com.concrete.onboarding.functions.extension

import br.com.concrete.onboarding.functions.Money
import java.math.BigDecimal

fun BigDecimal.percent(percentage: Double) = this
    .multiply(BigDecimal(percentage))
    .divide(BigDecimal(100))

infix fun Int.percentOf(money: Money) = money.amount
    .times(this)
    .div(100)

fun main(args: Array<String>) {

    val money = Money(500.0, "$")
    println(50.percentOf(money))

    println(50 percentOf money)
}
