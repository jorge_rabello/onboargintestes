package br.com.concrete.onboarding

fun isEven(number: Int) = number % 2 == 0

fun main(args: Array<String>) {

    println(isEven(10))

    println(isEven(3))

    val numbers = listOf(10, 20, 30, 3, 1, 2)

    println(numbers.filter(::isEven))

}
