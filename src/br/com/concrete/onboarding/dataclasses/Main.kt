package br.com.concrete.onboarding.dataclasses

fun main(args: Array<String>) {

    val javaMoney = JavaMoney(100.0, "USD")
    println("Amount: ${javaMoney.amount}")
    println("Currency: ${javaMoney.currency}")

}
