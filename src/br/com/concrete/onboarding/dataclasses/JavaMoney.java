package br.com.concrete.onboarding.dataclasses;

import java.util.Objects;

public class JavaMoney {

    private final Double amount;
    private final String currency;

    public JavaMoney(Double amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    @Override
    public String toString() {
        return "JavaMoney{" +
                "amount=" + amount +
                ", currency='" + currency + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JavaMoney javaMoney = (JavaMoney) o;
        return Objects.equals(amount, javaMoney.amount) &&
                Objects.equals(currency, javaMoney.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, currency);
    }
}
