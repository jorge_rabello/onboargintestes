package br.com.concrete.onboarding.dataclasses

data class Money(var amount: Double, val currency: String)
