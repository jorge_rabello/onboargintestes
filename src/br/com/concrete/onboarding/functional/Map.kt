package br.com.concrete.onboarding.functional

import br.com.concrete.onboarding.functions.userFromJSONFile

fun main(args: Array<String>) {

    val users = userFromJSONFile("users.json")

    val adults = users
        .filter { user -> user.age > 18 }
        .map { user -> user.name }

    println(adults)

    val names = users.map { it.name }

    val namesLength = names.map { it.length }

    println("Names $names\nSizes: $namesLength")
}
