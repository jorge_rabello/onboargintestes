package br.com.concrete.onboarding.functional

fun multiplicaPor2(number: Int): Int = number.times(2)

fun multiplicaLista(vararg numbers: Int): Int {

    return multiplicaPor2(numbers.reduce { acumulador, n ->
        acumulador.plus(n)
    })

}

fun main(args: Array<String>) {
    print(multiplicaLista(2, 3, 4))
}



