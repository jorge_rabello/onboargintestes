package br.com.concrete.onboarding.functional

fun oddNumbers(number: Int) = number % 2 != 0
fun evenNumbers(number: Int) = number % 2 == 0

fun main(args: Array<String>) {

    // lista xigante de números inteiros
    val alotOfNumbers = listOf<Int>(
        81, 25, 57, 11, 75, 73, 40, 85, 53, 96,
        46, 3, 4, 90, 73, 53, 19, 23, 54, 14,
        56, 10, 49, 90, 32, 71, 24, 79, 46, 41,
        20, 27, 66, 77, 38, 41, 49, 78, 26, 2,
        74, 71, 4, 77, 61, 77, 30, 80, 99, 83
    )

    val odds = alotOfNumbers.filter(::oddNumbers)

    val evens = alotOfNumbers.filter(::evenNumbers)

    println("Impares: ${odds.sorted()}")

    println("Pares: ${evens.sorted()}")
}
