package br.com.concrete.onboarding.functional

import br.com.concrete.onboarding.functions.userFromJSONFile

fun main(args: Array<String>) {
    val names = mutableListOf<String>()
    val namesLength = mutableListOf<Int>()

    val users = userFromJSONFile("users.json")

    users.forEach { user -> names.add(user.name) }

    names.forEach { name -> namesLength.add(name.length) }

    println(names.take(3))

    println("Names ${names.take(3)} and \nSizes: ${namesLength.take(3)}")

    println("Names ${names.take(3)} and \nSizes: ${namesLength.take(3).sorted()}")
}


